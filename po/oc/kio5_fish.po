# translation of kio_fish.po to Occitan (lengadocian)
# Occitan translation of kio_fish.po
# Copyright (C) 2003, 2004, 2007, 2008 Free Software Foundation, Inc.
#
# Yannig MARCHEGAY (Kokoyaya) <yannig@marchegay.org> - 2006-2007
#
# Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kio_fish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-26 00:46+0000\n"
"PO-Revision-Date: 2008-08-05 22:26+0200\n"
"Last-Translator: Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>\n"
"Language-Team: Occitan (lengadocian) <ubuntu-l10n-oci@lists.ubuntu.com>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: KBabel 1.11.4\n"

#: fish.cpp:289
#, kde-format
msgid "Connecting..."
msgstr "Connection..."

#: fish.cpp:600
#, kde-format
msgid "Initiating protocol..."
msgstr ""

#: fish.cpp:637
#, kde-format
msgid "Local Login"
msgstr ""

#: fish.cpp:639
#, kde-format
msgid "SSH Authentication"
msgstr ""

#: fish.cpp:676 fish.cpp:691
#, kde-format
msgctxt "@action:button"
msgid "Yes"
msgstr ""

#: fish.cpp:676 fish.cpp:691
#, kde-format
msgctxt "@action:button"
msgid "No"
msgstr ""

#: fish.cpp:768
#, kde-format
msgid "Disconnected."
msgstr "Desconnectat."
